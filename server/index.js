let express = require('express');
let app = express();
let router = express.Router();
let bodyParser = require('body-parser');

let port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let cars = [
    {id: 1, name: 'car #1', age: 5, state: 'good', on_repair: false, on_the_route: true},
    {id: 2, name: 'car #2', age: 2, state: 'excellent', on_repair: false, on_the_route: false},
    {id: 3, name: 'car #3', age: 2, state: 'excellent', on_repair: false, on_the_route: true},
    {id: 4, name: 'car #4', age: 5.5, state: 'excellent', on_repair: false, on_the_route: false},
    {id: 5, name: 'car #55', age: 15, state: 'acceptable', on_repair: false, on_the_route: true},
    {id: 6, name: 'car #7', age: 8, state: 'good', on_repair: false, on_the_route: true},
    {id: 7, name: 'car #8', age: null, state: 'acceptable', on_repair: false, on_the_route: true},
    {id: 8, name: 'car #15', age: 16, state: 'acceptable', on_repair: false, on_the_route: true},
    {id: 9, name: 'car #20', age: 18, state: 'bad', on_repair: false, on_the_route: true},
    {id: 10, name: 'car #16a', age: 20, state: 'bad', on_repair: true, on_the_route: false},
    {id: 11, name: 'car #16b', age: 6, state: 'bad', on_repair: false, on_the_route: true},
    {id: 12, name: 'car #16c', age: 10, state: 'excellent', on_repair: false, on_the_route: true},
    {id: 13, name: 'car #274', age: 7, state: 'good', on_repair: false, on_the_route: true},
    {id: 14, name: 'car #211', age: 8, state: 'good', on_repair: false, on_the_route: true},
    {id: 15, name: 'car #212', age: 7, state: 'good', on_repair: false, on_the_route: true},
    {id: 18, name: 'car #201', age: 7, state: 'good', on_repair: false, on_the_route: false},
    {id: 19, name: 'car #8c', age: 12, state: 'acceptable', on_repair: false, on_the_route: true},
    {id: 30, name: 'car #10', age: 12, state: 'acceptable', on_repair: true, on_the_route: true},
    {id: 32, name: 'car #11', age: 14, state: 'acceptable', on_repair: true, on_the_route: true},
    {id: 33, name: 'car #12', age: 15, state: 'acceptable', on_repair: true, on_the_route: true}
];


router.get('/', function (req, res) {
    res.json({message: 'hi rest-api!'});
});

router.route('/cars').get(function (req, res) {
    res.json(cars);
}).post(function (req, res) {
    let maxId = 0;
    cars.forEach(car => {
        if (car.id > maxId) {
            maxId = car.id;
        }
    });

    let newCar = req.body;
    newCar['id'] = maxId;

    cars.push(newCar);

    res.json({status: 'success'});
});

router.route('/cars/:id').get(function (req, res) {
    let car = cars.find(car => car.id === parseInt(req.params.id) );

    if (!car) {
        res.status(400).json({error: "can't find a car"});
    } else {
        res.json(car);
    }
}).put(function (req, res) {
    let car = cars.find(car => car.id === parseInt(req.params.id) );

    if (!car) {
        res.status(400).json({error: "can't find a car"});
    } else {
        car.name = req.body.name;
        car.age = req.body.age;
        car.state  = req.body.state;
        car.on_repair = req.body.on_repair;
        car.on_the_route  = req.body.on_the_route;

        res.json({status: 'success'});
    }
}).delete(function (req, res) {
    let index = cars.findIndex(car => car.id === parseInt(req.params.id) );
    if (index === -1) {
        res.status(400).json({error: "can't find a car"});
    } else {
        cars.splice(index, 1);

        res.json({status: 'success'});
    }
});

app.use('/api', router);

app.listen(port);
console.log('App started on port ' + port);